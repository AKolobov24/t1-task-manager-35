package ru.t1.akolobov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.service.IProjectService;
import ru.t1.akolobov.tm.comparartor.NameComparator;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.IndexIncorrectException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestProject.createProject;
import static ru.t1.akolobov.tm.data.TestProject.createProjectList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    private final IProjectRepository repository = new ProjectRepository();
    private final IProjectService service = new ProjectService(repository);

    @Before
    public void initRepository() {
        repository.add(createProjectList(USER1_ID));
    }

    @After
    public void clearRepository() {
        repository.clear(USER1_ID);
        repository.clear(USER2_ID);
    }

    @Test
    public void add() {
        Project project = createProject(USER1_ID);
        Object result = service.add(USER1_ID, project);
        Assert.assertNotNull(result);
        Assert.assertEquals(project, result);
        Assert.assertEquals(project, repository.findOneById(project.getUserId(), project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add("", project));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(""));
        List<Project> projectList = createProjectList(USER2_ID);
        service.add(projectList);
        Assert.assertFalse(service.findAll(USER2_ID).isEmpty());
        service.clear(USER2_ID);
        Assert.assertTrue(service.findAll(USER2_ID).isEmpty());
    }

    @Test
    public void existById() {
        Project project = createProject(USER1_ID);
        service.add(project);
        Assert.assertTrue(service.existById(USER1_ID, project.getId()));
        Assert.assertFalse(service.existById(USER2_ID, project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1_ID, USER_EMPTY_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(USER_EMPTY_ID, project.getId()));
    }

    @Test
    public void findAll() {
        List<Project> projectList = createProjectList(USER2_ID);
        service.add(projectList);
        Assert.assertEquals(projectList, service.findAll(USER2_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(USER_EMPTY_ID));
    }


    @Test
    public void findAllSorted() {
        @NotNull Project project = createProject(USER1_ID);
        project.setName("project-0");
        project.setDescription("project-0-desc");
        @NotNull List<Project> projectList = new ArrayList<>();
        projectList.add(project);
        projectList.addAll(service.findAll(USER1_ID));
        service.add(project);
        Assert.assertEquals(projectList, service.findAll(USER1_ID, NameComparator.INSTANCE));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findAll(USER_EMPTY_ID, NameComparator.INSTANCE)
        );
    }


    @Test
    public void findOneById() {
        @NotNull Project project = createProject(USER1_ID);
        service.add(project);
        Assert.assertEquals(project, service.findOneById(USER1_ID, project.getId()));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneById(USER_EMPTY_ID, project.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.findOneById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void findOneByIndex() {
        @NotNull Project project = createProject(USER1_ID);
        service.add(project);
        int indexForCheck = service.findAll(USER1_ID).indexOf(project);
        service.add(createProjectList(USER1_ID));
        Assert.assertEquals(project, service.findOneByIndex(USER1_ID, indexForCheck));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneByIndex(USER_EMPTY_ID, indexForCheck)
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> service.findOneByIndex(USER1_ID, service.findAll(USER1_ID).size())
        );
    }


    @Test
    public void getSize() {
        int size = service.findAll(USER1_ID).size();
        Assert.assertEquals(size, service.getSize(USER1_ID).intValue());
        service.add(createProject(USER1_ID));
        Assert.assertEquals(size + 1, service.getSize(USER1_ID).intValue());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(USER_EMPTY_ID));
    }

    /* TODO
    @Nullable
    Project remove(@NotNull String userId, @NotNull M model);

    @NotNull
    Project removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project removeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    Project updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @Nullable String description);

    @NotNull
    Project updateByIndex(@Nullable String userId,
                          @Nullable Integer index,
                          @Nullable String name,
                          @Nullable String description);

     */

}
