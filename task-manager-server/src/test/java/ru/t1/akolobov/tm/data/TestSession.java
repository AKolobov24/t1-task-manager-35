package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.Session;

import java.util.ArrayList;
import java.util.List;

public final class TestSession {

    @NotNull
    public static Session createSession(@NotNull final String userId) {
        @NotNull Session session = new Session();
        session.setUserId(userId);
        return session;
    }

    @NotNull
    public static List<Session> createSessionList(@NotNull final String userId) {
        @NotNull List<Session> sessionList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull Session session = new Session();
            session.setUserId(userId);
            sessionList.add(session);
        }
        return sessionList;
    }

}
