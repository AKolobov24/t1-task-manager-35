package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestProject.createProject;
import static ru.t1.akolobov.tm.data.TestProject.createProjectList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @Test
    public void add() {
        @NotNull IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull Project project = createProject();
        repository.add(USER1_ID, project);
        project.setUserId(USER1_ID);
        Assert.assertEquals(project, repository.findAll().get(0));
        repository.add(USER_EMPTY_ID, project);
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void clear() {
        @NotNull IProjectRepository repository = new ProjectRepository();
        List<Project> projectList = createProjectList(USER1_ID);
        repository.add(projectList);
        Project user2Project = repository.add(createProject(USER2_ID));
        Assert.assertEquals(projectList.size() + 1, repository.findAll().size());
        repository.clear(USER1_ID);
        Assert.assertEquals(1, repository.findAll().size());
        Assert.assertEquals(user2Project, repository.findAll().get(0));
    }

    @Test
    public void existById() {
        @NotNull IProjectRepository repository = new ProjectRepository();
        @NotNull String projectId = repository.add(createProject(USER1_ID)).getId();
        Assert.assertTrue(repository.existById(USER1_ID, projectId));
        Assert.assertFalse(repository.existById(USER2_ID, projectId));
    }

    @Test
    public void findAll() {
        @NotNull IProjectRepository repository = new ProjectRepository();
        List<Project> user1ProjectList = createProjectList(USER1_ID);
        List<Project> user2ProjectList = createProjectList(USER2_ID);
        repository.add(user1ProjectList);
        repository.add(user2ProjectList);
        Assert.assertEquals(user1ProjectList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2ProjectList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull IProjectRepository repository = new ProjectRepository();
        @NotNull Project project = repository.add(createProject(USER1_ID));
        @NotNull String projectId = project.getId();
        Assert.assertEquals(project, repository.findOneById(USER1_ID, projectId));
        Assert.assertNull(repository.findOneById(USER2_ID, projectId));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void findOneByIndex() {
        @NotNull IProjectRepository repository = new ProjectRepository();
        repository.add(createProjectList(USER1_ID));
        int lastIndex = repository.getSize() - 1;
        @NotNull Project project = repository.add(createProject(USER1_ID));
        Assert.assertEquals(project, repository.findOneByIndex(USER1_ID, lastIndex + 1));
        repository.findOneByIndex(USER1_ID, lastIndex + 2);
    }

    @Test
    public void getSize() {
        @NotNull IProjectRepository repository = new ProjectRepository();
        List<Project> projectList = createProjectList(USER1_ID);
        repository.add(projectList);
        Assert.assertEquals((Integer) projectList.size(), repository.getSize(USER1_ID));
        repository.add(createProject(USER1_ID));
        Assert.assertEquals((Integer) (projectList.size() + 1), repository.getSize(USER1_ID));
    }

    @Test
    public void remove() {
        @NotNull IProjectRepository repository = new ProjectRepository();
        repository.add(createProjectList(USER1_ID));
        @NotNull Project project = repository.add(createProject(USER1_ID));
        Assert.assertNull(repository.remove(USER_EMPTY_ID, project));
        Assert.assertEquals(project, repository.findOneById(USER1_ID, project.getId()));
        repository.remove(USER1_ID, project);
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() {
        @NotNull IProjectRepository repository = new ProjectRepository();
        repository.add(createProjectList(USER1_ID));
        @NotNull Project project = repository.add(createProject(USER1_ID));
        repository.removeById(USER1_ID, project.getId());
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
        repository.removeById(USER2_ID, project.getId());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByIndex() {
        @NotNull IProjectRepository repository = new ProjectRepository();
        List<Project> projectList = createProjectList(USER1_ID);
        repository.add(projectList);
        int indexForCheck = projectList.size() - 2;
        @NotNull Project project = repository.findOneByIndex(USER1_ID, indexForCheck);
        Assert.assertEquals(project, repository.removeByIndex(USER1_ID, indexForCheck));
        Assert.assertEquals((Integer) (projectList.size() - 1), repository.getSize());
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
        repository.removeByIndex(USER1_ID, projectList.size());
    }

}
