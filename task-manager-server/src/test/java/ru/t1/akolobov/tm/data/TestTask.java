package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TestTask {

    @NotNull
    public static Task createTask() {
        return new Task("task-1", "task-1-desc");
    }

    @NotNull
    public static Task createTask(@NotNull final String userId) {
        @NotNull Task task = createTask();
        task.setUserId(userId);
        return task;
    }

    @NotNull
    public static List<Task> createTaskList(@NotNull final String userId) {
        @NotNull List<Task> taskList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull Task task = new Task("task-" + i, "task-" + i + "desc");
            task.setUserId(userId);
            taskList.add(task);
        }
        return taskList;
    }

}
