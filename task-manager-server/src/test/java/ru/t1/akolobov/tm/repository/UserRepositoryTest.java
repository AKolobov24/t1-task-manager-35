package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.User;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @Test
    public void add() {
        @NotNull IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findAll().get(0));
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void clear() {
        @NotNull IUserRepository repository = new UserRepository();
        List<User> userList = createUserList();
        repository.add(userList);
        Assert.assertEquals(userList.size(), repository.findAll().size());
        repository.clear();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void existById() {
        @NotNull IUserRepository repository = new UserRepository();
        @NotNull String userId = repository.add(USER1).getId();
        Assert.assertTrue(repository.existById(userId));
        Assert.assertFalse(repository.existById(USER2.getId()));
    }

    @Test
    public void findAll() {
        @NotNull IUserRepository repository = new UserRepository();
        List<User> user1UserList = createUserList();
        repository.add(user1UserList);
        Assert.assertEquals(user1UserList, repository.findAll());
    }

    @Test
    public void findOneById() {
        @NotNull IUserRepository repository = new UserRepository();
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
        Assert.assertNull(repository.findOneById(USER2.getId()));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void findOneByIndex() {
        @NotNull IUserRepository repository = new UserRepository();
        repository.add(createUserList());
        int lastIndex = repository.getSize() - 1;
        repository.add(NEW_USER);
        Assert.assertEquals(NEW_USER, repository.findOneByIndex(lastIndex + 1));
        repository.findOneByIndex(lastIndex + 2);
    }

    @Test
    public void getSize() {
        @NotNull IUserRepository repository = new UserRepository();
        List<User> userList = createUserList();
        repository.add(userList);
        Assert.assertEquals(userList.size(), repository.getSize().intValue());
        repository.add(NEW_USER);
        Assert.assertEquals((userList.size() + 1), repository.getSize().intValue());
    }

    @Test
    public void remove() {
        @NotNull IUserRepository repository = new UserRepository();
        repository.add(createUserList());
        repository.add(NEW_USER);
        Assert.assertEquals(NEW_USER, repository.findOneById(NEW_USER.getId()));
        Integer size = repository.getSize();
        repository.remove(NEW_USER);
        Assert.assertNull(repository.findOneById(NEW_USER.getId()));
        Assert.assertEquals(size - 1, repository.getSize().intValue());
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeById() {
        @NotNull IUserRepository repository = new UserRepository();
        repository.add(createUserList());
        repository.add(NEW_USER);
        repository.removeById(NEW_USER.getId());
        Assert.assertNull(repository.findOneById(NEW_USER.getId()));
        repository.removeById(NEW_USER.getId());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByIndex() {
        @NotNull IUserRepository repository = new UserRepository();
        List<User> userList = createUserList();
        repository.add(userList);
        int indexForCheck = Math.max((userList.size() - 2), 0);
        @NotNull User user = repository.findOneByIndex(indexForCheck);
        Assert.assertEquals(user, repository.removeByIndex(indexForCheck));
        Assert.assertEquals(userList.size() - 1, repository.getSize().intValue());
        Assert.assertNull(repository.findOneById(user.getId()));
        repository.removeByIndex(userList.size());
    }

    @Test
    public void findByLogin() {
        @NotNull IUserRepository repository = new UserRepository();
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findByLogin(USER1.getLogin()));
        Assert.assertNull(repository.findOneById(USER2.getLogin()));
    }

    @Test
    public void findByEmail() {
        @NotNull IUserRepository repository = new UserRepository();
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findByEmail(USER1.getEmail()));
        Assert.assertNull(repository.findByEmail("user_2@email.ru"));

    }

    @Test
    public void isLoginExist() {
        @NotNull IUserRepository repository = new UserRepository();
        repository.add(USER1);
        Assert.assertTrue(repository.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(repository.isLoginExist(USER2.getLogin()));
    }

    @Test
    public void isEmailExist() {
        @NotNull IUserRepository repository = new UserRepository();
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.add(USER1);
        Assert.assertTrue(repository.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(repository.isEmailExist("user_2@email.ru"));
    }

}
