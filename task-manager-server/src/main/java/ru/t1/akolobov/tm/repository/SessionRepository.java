package ru.t1.akolobov.tm.repository;

import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}
