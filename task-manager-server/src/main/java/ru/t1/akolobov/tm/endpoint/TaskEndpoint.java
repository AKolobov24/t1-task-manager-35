package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.akolobov.tm.api.service.IProjectTaskService;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.api.service.ITaskService;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;
import ru.t1.akolobov.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.akolobov.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectTaskService projectTaskService;

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.taskService = getServiceLocator().getTaskService();
        this.projectTaskService = getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectTaskService.bindTaskToProject(
                userId,
                request.getProjectId(),
                request.getTaskId()
        );
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectTaskService.unbindTaskFromProject(
                userId,
                request.getId()
        );
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskGetByProjectIdResponse getByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByProjectIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new TaskGetByProjectIdResponse(
                taskService.findAllByProjectId(
                        userId,
                        request.getProjectId()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        @NotNull final String userId = check(request).getUserId();
        taskService.changeStatusById(
                userId,
                request.getId(),
                request.getStatus()
        );
        return new TaskChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIndexRequest request) {
        @NotNull final String userId = check(request).getUserId();
        taskService.changeStatusByIndex(
                userId,
                request.getIndex(),
                request.getStatus()
        );
        return new TaskChangeStatusByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.changeStatusById(
                userId,
                request.getId(),
                Status.COMPLETED
        );
        return new TaskCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIndexResponse completeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.changeStatusByIndex(
                userId,
                request.getIndex(),
                Status.COMPLETED
        );
        return new TaskCompleteByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new TaskCreateResponse(
                taskService.create(
                        userId,
                        request.getName(),
                        request.getDescription()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public TaskGetByIdResponse getTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new TaskGetByIdResponse(
                taskService.findOneById(
                        userId,
                        request.getId())
        );
    }

    @NotNull
    @Override
    @WebMethod
    public TaskGetByIndexResponse getTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new TaskGetByIndexResponse(
                taskService.findOneByIndex(
                        userId,
                        request.getIndex()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new TaskListResponse(
                taskService.findAll(
                        userId,
                        request.getSortType()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.removeById(userId, request.getId());
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.removeByIndex(userId, request.getIndex());
        return new TaskRemoveByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.changeStatusById(
                userId,
                request.getId(),
                Status.IN_PROGRESS
        );
        return new TaskStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIndexResponse startTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.changeStatusByIndex(
                userId,
                request.getIndex(),
                Status.IN_PROGRESS
        );
        return new TaskStartByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.updateById(
                userId,
                request.getId(),
                request.getName(),
                request.getDescription()
        );
        return new TaskUpdateByIdResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        taskService.updateByIndex(
                userId,
                request.getIndex(),
                request.getName(),
                request.getDescription()
        );
        return new TaskUpdateByIndexResponse();
    }

}
