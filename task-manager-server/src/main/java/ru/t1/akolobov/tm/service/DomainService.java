package ru.t1.akolobov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.service.IDomainService;
import ru.t1.akolobov.tm.api.service.IProjectService;
import ru.t1.akolobov.tm.api.service.ITaskService;
import ru.t1.akolobov.tm.api.service.IUserService;
import ru.t1.akolobov.tm.dto.Domain;
import ru.t1.akolobov.tm.enumerated.FileFormat;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

public final class DomainService implements IDomainService {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";
    @NotNull
    public static final String FILE_BINARY = "./data.bin";
    @NotNull
    public static final String FILE_XML = "./data.xml";
    @NotNull
    public static final String FILE_JSON = "./data.json";
    @NotNull
    public static final String FILE_YAML = "./data.yaml";
    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";
    @NotNull
    public final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";
    @NotNull
    public final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";
    @NotNull
    public final String MEDIA_TYPE = "eclipselink.media-type";
    @NotNull
    public final String APPLICATION_TYPE_JSON = "application/json";
    @NotNull
    private final IProjectService projectService;
    @NotNull
    private final ITaskService taskService;
    @NotNull
    private final IUserService userService;

    public DomainService(
            @NotNull IProjectService projectService,
            @NotNull ITaskService taskService,
            @NotNull IUserService userService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @NotNull
    private Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setUsers(userService.findAll());
        return domain;
    }

    private void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        projectService.set(domain.getProjects());
        taskService.set(domain.getTasks());
        userService.set(domain.getUsers());
    }

    @Override
    @NotNull
    public String getBackupFilePath() {
        return FILE_BACKUP;
    }

    @Override
    @SneakyThrows
    public void saveDataBackup() {
        @NotNull Domain domain = getDomain();
        @NotNull File file = new File(FILE_BACKUP);
        @NotNull Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataBase64() {
        @NotNull Domain domain = getDomain();
        @NotNull File file = new File(FILE_BASE64);
        @NotNull Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataBinary() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveData(FileFormat format) {
        @NotNull final File file;
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper;

        switch (format) {
            case XML:
                file = new File(FILE_XML);
                objectMapper = new XmlMapper();
                break;
            case YAML:
                file = new File(FILE_YAML);
                objectMapper = new YAMLMapper();
                break;
            case JSON:
            default:
                file = new File(FILE_JSON);
                objectMapper = new ObjectMapper();
        }

        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final String dataString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(dataString.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataJaxb(FileFormat format) {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file;
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        switch (format) {
            case XML:
                file = new File(FILE_XML);
                break;
            case JSON:
            default:
                file = new File(FILE_JSON);
                System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
                jaxbMarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
        }

        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataBackup() {
        final byte[] base64Bytes = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @Nullable final String base64String = new String(base64Bytes);
        final byte[] bytes = Base64.getDecoder().decode(base64String);

        @NotNull ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataBase64() {
        final byte[] base64Bytes = Files.readAllBytes(Paths.get(FILE_BASE64));
        @Nullable final String base64String = new String(base64Bytes);
        final byte[] bytes = Base64.getDecoder().decode(base64String);

        @NotNull ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataBinary() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadData(@NotNull FileFormat format) {
        @NotNull final String path;
        @NotNull final ObjectMapper objectMapper;
        switch (format) {
            case XML:
                path = FILE_XML;
                objectMapper = new XmlMapper();
                break;
            case YAML:
                path = FILE_YAML;
                objectMapper = new YAMLMapper();
                break;
            case JSON:
            default:
                path = FILE_JSON;
                objectMapper = new ObjectMapper();
        }

        final byte[] bytes = Files.readAllBytes(Paths.get(path));
        @NotNull final String bytesString = new String(bytes);
        @NotNull final Domain domain = objectMapper.readValue(bytesString, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataJaxb(FileFormat format) {
        @NotNull String path;
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        switch (format) {
            case XML:
                path = FILE_XML;
                break;
            case JSON:
            default:
                path = FILE_JSON;
                System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
                unmarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
        }

        @NotNull final File file = new File(path);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

}
