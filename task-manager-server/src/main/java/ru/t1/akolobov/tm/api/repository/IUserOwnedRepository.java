package ru.t1.akolobov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @Nullable
    M add(@NotNull String userId, @NotNull M model);

    void clear(@NotNull String userId);

    boolean existById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<? super M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Integer getSize(@NotNull String userId);

    @Nullable
    M remove(@NotNull String userId, @NotNull M model);

    @NotNull
    M removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    M removeByIndex(@NotNull String userId, @NotNull Integer index);

}
