package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
