package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

}
