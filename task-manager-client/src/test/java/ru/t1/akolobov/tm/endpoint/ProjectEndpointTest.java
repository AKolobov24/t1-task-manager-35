package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.akolobov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.ProjectCreateResponse;
import ru.t1.akolobov.tm.dto.response.ProjectGetByIdResponse;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.marker.IntegrationCategory;
import ru.t1.akolobov.tm.model.Project;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.ArrayList;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestProject.createProject;
import static ru.t1.akolobov.tm.data.TestProject.createProjectList;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance("localhost", "6060");
    IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance("localhost", "6060");

    String adminToken;
    String userToken;
    List<Project> projectList = createProjectList(4);
    List<Project> serverProjectList = new ArrayList<>();

    @Before
    public void prepareSession() {
        adminToken = authEndpoint.login(new UserLoginRequest("akolobov", "akolobov")).getToken();
        userToken = authEndpoint.login(new UserLoginRequest("user1", "user1")).getToken();
        ProjectCreateRequest request = new ProjectCreateRequest(userToken);
        for (int i = 0; i < 4; i++) {
            if (i > 2) request.setToken(adminToken);
            request.setName(projectList.get(i).getName());
            request.setDescription(projectList.get(i).getDescription());
            serverProjectList.add(projectEndpoint.create(request).getProject());
        }
    }

    @After
    public void closeSessions() {
        authEndpoint.logout(new UserLogoutRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(userToken));
        adminToken = null;
        userToken = null;

    }

    @Test
    public void changeStatusById() {
        @NotNull ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(adminToken);
        @NotNull Project project = serverProjectList.get(3);
        request.setId(project.getId());
        request.setStatus(Status.IN_PROGRESS);
        projectEndpoint.changeStatusById(request);
        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(adminToken);
        getByIdRequest.setId(project.getId());
        @NotNull ProjectGetByIdResponse response = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        // TODO
    }

    @Test
    public void clear() {
        @NotNull ProjectListRequest projectListRequest = new ProjectListRequest(userToken);
        Assert.assertFalse(projectEndpoint.list(projectListRequest).getProjectList().isEmpty());
        projectEndpoint.clear(new ProjectClearRequest(userToken));
        Assert.assertTrue(projectEndpoint.list(projectListRequest).getProjectList().isEmpty());
        Assert.assertFalse(projectEndpoint.list(new ProjectListRequest(adminToken)).getProjectList().isEmpty());
        Assert.assertThrows(SOAPFaultException.class, () -> projectEndpoint.clear(new ProjectClearRequest()));
    }

    @Test
    public void completeById() {
        // TODO
    }

    @Test
    public void completeByIndex() {
        // TODO
    }

    @Test
    public void create() {
        @NotNull Project project = createProject();
        @NotNull ProjectCreateRequest request = new ProjectCreateRequest(userToken);
        request.setName(project.getName());
        request.setDescription(project.getDescription());

        @NotNull ProjectCreateResponse response = projectEndpoint.create(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(project.getName(), response.getProject().getName());
        Assert.assertEquals(project.getDescription(), response.getProject().getDescription());

        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(userToken);
        getByIdRequest.setId(response.getProject().getId());
        @NotNull ProjectGetByIdResponse getByIdResponse = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(getByIdResponse.getProject());
        Assert.assertEquals(project.getName(), getByIdResponse.getProject().getName());
        Assert.assertEquals(project.getDescription(), getByIdResponse.getProject().getDescription());

        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.create(new ProjectCreateRequest())
        );
    }

    @Test
    public void getById() {
        @NotNull Project project = createProject();
        @NotNull ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken);
        createRequest.setName(project.getName());
        createRequest.setDescription(project.getDescription());

        @NotNull ProjectCreateResponse createResponse = projectEndpoint.create(createRequest);
        Assert.assertNotNull(createResponse.getProject());

        @NotNull ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(userToken);
        getByIdRequest.setId(createResponse.getProject().getId());
        @NotNull ProjectGetByIdResponse getByIdResponse = projectEndpoint.getById(getByIdRequest);

        Assert.assertNotNull(getByIdResponse.getProject());
        Assert.assertEquals(createResponse.getProject().getId(), getByIdResponse.getProject().getId());
        Assert.assertEquals(project.getName(), getByIdResponse.getProject().getName());
        Assert.assertEquals(project.getDescription(), getByIdResponse.getProject().getDescription());

        getByIdRequest.setToken(adminToken);
        Assert.assertNull(projectEndpoint.getById(getByIdRequest).getProject());
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.getById(new ProjectGetByIdRequest())
        );
    }

    @Test
    public void getByIndex() {
        // TODO
    }

    @Test
    public void list() {
        ProjectListRequest userProjectListRequest = new ProjectListRequest(userToken);
        ProjectListRequest adminProjectListRequest = new ProjectListRequest(adminToken);
        int userProjectListSize = projectEndpoint
                .list(userProjectListRequest)
                .getProjectList().size();
        int adminProjectListSize = projectEndpoint
                .list(adminProjectListRequest)
                .getProjectList().size();

        @NotNull Project project = createProject();
        @NotNull ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken);
        createRequest.setName(project.getName());
        createRequest.setDescription(project.getDescription());

        @NotNull ProjectCreateResponse createResponse = projectEndpoint.create(createRequest);
        Project createdProject = createResponse.getProject();
        Assert.assertNotNull(createdProject);

        List<Project> projectList = projectEndpoint.list(userProjectListRequest).getProjectList();

        Assert.assertEquals(
                userProjectListSize + 1,
                projectList.size()
        );

        Assert.assertEquals(
                adminProjectListSize,
                projectEndpoint.list(adminProjectListRequest).getProjectList().size()
        );

        Assert.assertTrue(
                projectList.stream()
                        .anyMatch(m -> createdProject.getId().equals(m.getId()))
        );
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.list(new ProjectListRequest())
        );
    }

    @Test
    public void removeById() {
        ProjectGetByIndexRequest projectGetByIndexRequest = new ProjectGetByIndexRequest(userToken);
        projectGetByIndexRequest.setIndex(0);
        Project project = projectEndpoint.getByIndex(projectGetByIndexRequest).getProject();
        Assert.assertNotNull(project);

        ProjectRemoveByIdRequest projectRemoveByIdRequest = new ProjectRemoveByIdRequest(userToken);
        projectRemoveByIdRequest.setId(project.getId());
        projectEndpoint.removeById(projectRemoveByIdRequest);
        Assert.assertFalse(projectEndpoint
                .list(new ProjectListRequest(userToken))
                .getProjectList()
                .contains(project)
        );
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.removeById(new ProjectRemoveByIdRequest())
        );
    }

    @Test
    public void removeByIndex() {
        // TODO
    }

    @Test
    public void startById() {
        // TODO
    }

    @Test
    public void startByIndex() {
        // TODO
    }

    @Test
    public void updateById() {
        @NotNull Project project = createProject();
        @NotNull ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken);
        createRequest.setName(project.getName());
        createRequest.setDescription(project.getDescription());
        @NotNull ProjectCreateResponse createResponse = projectEndpoint.create(createRequest);
        Assert.assertNotNull(createResponse.getProject());

        String name = "update-by-id-name";
        String description = "update-by-id-desc";
        ProjectUpdateByIdRequest updateByIdRequest = new ProjectUpdateByIdRequest(userToken);
        updateByIdRequest.setId(createResponse.getProject().getId());
        updateByIdRequest.setName(name);
        updateByIdRequest.setDescription(description);
        projectEndpoint.updateById(updateByIdRequest);

        ProjectGetByIdRequest getByIdRequest = new ProjectGetByIdRequest(userToken);
        getByIdRequest.setId(createResponse.getProject().getId());
        ProjectGetByIdResponse getByIdResponse = projectEndpoint.getById(getByIdRequest);
        Assert.assertNotNull(getByIdResponse.getProject());
        Assert.assertEquals(name, getByIdResponse.getProject().getName());
        Assert.assertEquals(description, getByIdResponse.getProject().getDescription());

        Assert.assertThrows(
                SOAPFaultException.class,
                () -> projectEndpoint.updateById(new ProjectUpdateByIdRequest())
        );
        updateByIdRequest.setId(project.getId());
        Assert.assertThrows(SOAPFaultException.class,
                () -> projectEndpoint.updateById(updateByIdRequest)
        );
    }

    @Test
    public void updateByIndex() {
        // TODO
    }

}
